async function getFileAsString(filePath) {
    const response = await fetch(filePath)
    return response.text()
}

async function setUpTemplate() {
    // const styleCSS = await getFileAsString(process.env.STYLE)
    // const devHtml = await getFileAsString(process.env.DEV_HTML)
    return `
    <style>
    ${process.env.STYLE}
    </style>
    ${process.env.DEV_HTML}
    `
}

export class PaymentForm extends HTMLElement {
    constructor() {
        super()
        const shadowRoot = this.attachShadow({ mode: "open" })
        const template = document.createElement("template")

        setTimeout(async () => {
            template.innerHTML = await setUpTemplate()
            this.node = template.content.cloneNode(true)
            shadowRoot.appendChild(this.node)

            this.onload()
        })

        this.getOriginPage()
    }
    connectedCallback() {
        this.amount = parseFloat(this.getAttribute("amount")) || 0
        this.merchantInternalId = this.getAttribute("merchantInternalId") || ""
        this.sendEmailText = this.getAttribute("sendEmailText") || "no"
        this.language = this.getAttribute("language") || "eng"
        this.paymentID = this.getAttribute("paymentID") || ""
        this.termAndCondition = this.getAttribute("termAndCondition") || ""
        this.privacyPolicy = this.getAttribute("privacyPolicy") || ""
        this.buttonStyle = this.getAttribute("buttonStyle") || ""
        this.buttonHoverStyle = this.getAttribute("buttonHoverStyle") || ""
        this.buttonText = this.getAttribute("buttonText") || ""
        this.redirect = this.getAttribute("redirect") || "NONE"
    }
    mouseOver() {
        this.shadowRoot.querySelector("[data-pay_button]").style = "padding: 50px;"
    }
    onload() {
        const paymentButtonSelector = "[data-pay_button]"
        this.shadowRoot.querySelector(paymentButtonSelector).style = this.buttonStyle
        this.shadowRoot.querySelector(paymentButtonSelector).addEventListener("mouseover", () => {
            this.shadowRoot.querySelector(paymentButtonSelector).style = this.buttonStyle + this.buttonHoverStyle
        })
        this.shadowRoot.querySelector(paymentButtonSelector).addEventListener("mouseout", () => {
            this.shadowRoot.querySelector(paymentButtonSelector).style = this.buttonStyle
        })

        this.shadowRoot.querySelector(paymentButtonSelector).innerText = this.buttonText
        this.shadowRoot.querySelectorAll("[data-close_modal]").forEach((btn) => {
            btn.addEventListener("click", () => {
                this.closePopup()
            })
        })
        this.shadowRoot.querySelector(paymentButtonSelector).addEventListener("click", () => {
            this.openPopup()
        })

        this.shadowRoot.querySelector('[data-next_button="main_done"]').addEventListener("click", () => {
            const theAreErrors = this.mainInformationDoneGotoDisclaimer()
            console.log("isChecked ", this.disclaimer_checkbox.checked)

            if (this.disclaimer_checkbox.checked == false) {
                alert("Pleas read and agree to terms and conditions")
            } else {
                if (theAreErrors == false) {
                    this.shadowRoot.querySelector(".first_step").style.display = "none"
                    this.shadowRoot.querySelector(".payment-title").style.display = "none"
                    this.Pay()
                }
            }
        })
    }
    def_object() {
        return {
            redirect: this.redirect,
            amount: this.amount,
            uid: this.paymentID,
            origin: this.origin,
            sendEmailText: this.sendEmailText,
            lang: this.language,
        }
    }

    async openPopup() {
        new PaymentForm()
        try {
            const { merchantName, modaltoken, ipdetect } = await this.getMercahntDetails()

            if (merchantName != undefined && modaltoken != undefined && ipdetect != undefined) {
                this.toSendParamiters = this.def_object()
                this.merchantName = merchantName
                this.makePaymentToken = modaltoken
                this.ipdetect = ipdetect

                this.shadowRoot.querySelector(".modal").style.display = "block"
                this.shadowRoot.querySelector(".first_step").style.display = "block"
                this.shadowRoot.querySelector("[data-terms]").setAttribute("href", this.termAndCondition)
                this.shadowRoot.querySelector("[data-policy]").setAttribute("href", this.privacyPolicy)
                this.shadowRoot.querySelectorAll("[data-merchant]").forEach((field) => {
                    field.innerText = this.merchantName
                })
                this.disclaimer_checkbox = this.shadowRoot.querySelector("[data-disclaimer_chackbox ]")

                const allRequiredInputs = this.shadowRoot.querySelectorAll("[data-paramiters]")
                for (let i = 0; i < allRequiredInputs.length; i++) {
                    const element = allRequiredInputs[i]
                    element.value = ""
                    if (element.classList.contains("red")) {
                        element.classList.remove("red")
                    }
                    if (element.classList.contains("green")) {
                        element.classList.remove("green")
                    }
                }
            }
        } catch (error) {
            console.error("Payment window error")
        }
    }
    getOriginPage() {
        const queryString = window.location.search
        const urlParams = new URLSearchParams(queryString)
        let origin = urlParams.get("origin")
        if (!origin) {
            origin = document.referrer
        }
        this.origin = origin || ""
    }
    async getIpAddress() {
        const data = await fetch(`https://api.ipgeolocation.io/ipgeo?apiKey=${this.ipdetect}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        })
        const ipaddr = await data.json()
        return ipaddr.ip
    }
    async getMercahntDetails() {
        try {
            const data = await fetch(`${process.env.HOST}/api/v1/getMerchantName`, {
                method: "POST",
                headers: {
                    Authorization: `merchantauth ${this.merchantInternalId}`,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    uid: this.paymentID,
                }),
            })
            const resultData = await data.json()
            console.log(resultData.error)
            if (resultData.error == "") {
                return { merchantName: resultData.merchantName, modaltoken: resultData.modaltoken, ipdetect: resultData.ipdetect }
            } else {
                console.error(resultData.error)
            }
        } catch (error) {
            console.log(error)
        }
    }

    async Pay() {
        this.shadowRoot.querySelector(".loader-wrapper").classList.remove("none")

        const data = await fetch(`${process.env.HOST}/api/v1/payment_page`, {
            method: "POST",
            headers: {
                Authorization: `merchantauth ${this.makePaymentToken}`,
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ...this.toSendParamiters,
                clientIp: await this.getIpAddress(),
            }),
        })
        const createPaymentPageResponce = await data.json()

        const checkInterval = setInterval(async () => {
            try {
                const checkStatus = await fetch(`${process.env.HOST}/api/v1/checkstatus`, {
                    method: "POST",
                    headers: {
                        Authorization: `merchantauth ${this.makePaymentToken}`,
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        RequestId: createPaymentPageResponce.RequestId,
                    }),
                })

                const { status } = await checkStatus.json()
                if (status == "captured") {
                    clearInterval(checkInterval)
                    this.shadowRoot.querySelector(".loader-wrapper").classList.add("none")
                    this.shadowRoot.querySelector("[data-order_number]").innerText = this.paymentID
                    this.shadowRoot.querySelector(".payment_done").classList.remove("none")

                    if (this.redirect != "NONE") {
                        setTimeout(() => {
                            window.location.replace(this.redirect)
                        }, 5000)
                    }
                }
                if (status == "timeout canceled") {
                    clearInterval(checkInterval)
                    alert("Payment Time out please try again")
                    setTimeout(() => {
                        window.location.reload()
                    }, 1500)
                }
            } catch (error) {
                console.log(error)
            }
        }, 2000)

        if (this.sendEmailText == "yes") {
            alert("Please check your email for payment")
        } else {
            if (createPaymentPageResponce.PageUrl != "") {
                window.open(createPaymentPageResponce.PageUrl, "_blank")
            } else {
                alert("Error during payment plese try again, the page will be reloaded")
                window.location.reload()
            }
        }
    }

    mainInformationDoneGotoDisclaimer() {
        const allRequiredInputs = this.shadowRoot.querySelectorAll("[data-paramiters]")
        let errors = false
        for (let i = 0; i < allRequiredInputs.length; i++) {
            const element = allRequiredInputs[i]
            if (element.value == "") {
                if (!element.classList.contains("red")) {
                    element.classList.add("red")
                }
                errors = true
            } else {
                if (element.dataset.paramiters == "email") {
                    if (!this.validateEmail(element.value)) {
                        if (!element.classList.contains("red")) {
                            element.classList.add("red")
                        }
                        errors = true
                    } else {
                        if (element.classList.contains("red")) {
                            element.classList.remove("red")
                        }
                        if (!element.classList.contains("green")) {
                            element.classList.add("green")
                        }
                    }
                } else if (element.dataset.paramiters == "phone") {
                    if (!this.phonenumber(element.value)) {
                        if (!element.classList.contains("red")) {
                            element.classList.add("red")
                        }
                        errors = true
                    } else {
                        if (element.classList.contains("red")) {
                            element.classList.remove("red")
                        }
                        if (!element.classList.contains("green")) {
                            element.classList.add("green")
                        }
                    }
                } else {
                    if (element.classList.contains("red")) {
                        element.classList.remove("red")
                    }
                    if (!element.classList.contains("green")) {
                        element.classList.add("green")
                    }
                }
            }
            this.toSendParamiters[element.dataset.paramiters] = element.value
        }
        return errors
    }
    phonenumber(inputtxt) {
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
        if (inputtxt.match(phoneno)) {
            return true
        } else {
            return false
        }
    }

    validateEmail(input) {
        const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/

        if (input.match(validRegex)) {
            return true
        } else {
            return false
        }
    }

    closePopup() {
        this.shadowRoot.querySelector(".modal").style.display = "none"
    }
}
