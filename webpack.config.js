const path = require("path")
const fs = require("fs")
// Host
const webpack = require("webpack")

const styleFile = fs.readFileSync("./src/style.css", "utf8")
const dev_html = fs.readFileSync("./src/devHTML.html", "utf8")

module.exports = {
    mode: "production",
    entry: ["./src/index.js"],
    output: {
        filename: "escrowfy_pay.js",
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                include: path.resolve(__dirname, "src"),
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env"],
                    },
                },
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env.HOST": JSON.stringify(process.env.HOST),
            "process.env.STYLE": JSON.stringify(styleFile),
            "process.env.DEV_HTML": JSON.stringify(dev_html),
        }),
    ],
    devServer: {
        // Serve index.html as the base
        static: {
            directory: path.join(__dirname, "/src/"),
        },
        // Enable compression
        compress: true,

        // Enable hot reloading
        hot: true,
        host: "localhost",

        port: 3001,
    },
}
